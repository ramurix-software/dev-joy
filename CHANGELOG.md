# Changelog for v1.7

## 1.1.0 (2024-02-28)

### Enhancements
  * Added `CHANGELOG.md` file
  * Added optional and translatable `page_title` field support for `Part` struct and its `DSL`
  * Added `t:DevJoy.Gettext.fields_to_translate/0`
  * Added `DevJoy.Scene.Chapter` struct and its `DSL`

### Bug fixes
  * Added missing `export` option to `.formatter.exs` file allowing to use `locals_without_parens` options when importing `dev_joy`
  * Fixed various `:ets` related bugs by replacing its usage with `GenServer`-based solution
