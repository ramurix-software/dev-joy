[
  export: [
    locals_without_parens: [asset: 1, asset: 2, challenge: 1, challenge: 2, chapter: 3, choice: 2, dialog: 2, dialog: 3]
  ],
  inputs: ["*.{heex,ex,exs}", "{config,lib,test}/**/*.{heex,ex,exs}"],
  line_length: 100,
  locals_without_parens: [asset: 1, asset: 2, challenge: 1, challenge: 2, chapter: 3, choice: 2, dialog: 2, dialog: 3]
]
