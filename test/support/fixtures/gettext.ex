defmodule DevJoy.Fixtures.Gettext do
  @moduledoc false

  use Gettext, otp_app: :dev_joy, priv: "test/priv/gettext"
end
