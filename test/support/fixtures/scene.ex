defmodule DevJoy.Fixtures.Scene do
  @moduledoc false

  use DevJoy.Scene

  alias DevJoy.Fixtures.Scene

  part :asset, "Asset" do
    asset "/path/to/asset", some: :data
  end

  part :challenge, "Challenge" do
    challenge :challenge_type, some: :data
  end

  part :chapter, "Chapter" do
    chapter 1, "Chapter title", "Chapter description"
  end

  part :condition, "Condition" do
    condition :john_doe, &func/2 do
      choice :choice_name, goto(:empty)
    end
  end

  part :dialog, "Dialog box" do
    dialog :john_doe, "Example dialog"
  end

  part :empty, "Empty" do
  end

  part :goto, "Go to" do
    goto()
  end

  part :main, "Main" do
    question :john_doe, "Go to?" do
      choice "No arguments", goto()
      choice "Part name", goto(:main)
      choice "Module and part name", goto(Scene, :main)
    end
  end

  part :menu, "Menu" do
    menu "Menu title" do
      choice "Menu item content", goto(:empty)
    end
  end

  part :question, "Question" do
    question :john_doe, "Example question" do
      choice "Example answer", goto(:empty)
    end
  end

  defp func(_character, [choice]), do: choice.action.()
end
