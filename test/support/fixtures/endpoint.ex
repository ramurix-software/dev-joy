defmodule DevJoy.Fixtures.Endpoint do
  @moduledoc false

  @doc false
  @spec static_path(String.t()) :: String.t()
  def static_path(path), do: Path.join("/assets", path)
end
