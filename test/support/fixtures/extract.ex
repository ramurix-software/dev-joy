defmodule DevJoy.Fixtures.Extract.Gettext do
  @moduledoc false

  use Gettext, otp_app: :dev_joy, priv: "tmp/extract-test"
end

defmodule DevJoy.Fixtures.Extract.Scene do
  @moduledoc false

  use DevJoy.Scene, gettext_module: DevJoy.Fixtures.Extract.Gettext

  part :dialog, "Dialog box" do
    dialog :john_doe, "Example dialog"
  end
end
