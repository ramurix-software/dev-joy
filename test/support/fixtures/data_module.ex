defmodule DevJoy.Fixtures.DataModule do
  @moduledoc false

  @behaviour DevJoy.Character

  alias DevJoy.Character

  @impl Character
  def get_character(:john_doe) do
    [avatar: "example.com/john_doe.ico", full_name: "John Doe"]
  end
end
