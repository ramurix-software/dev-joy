defmodule DevJoy.Fixtures.ExtraModule do
  @moduledoc false

  @behaviour DevJoy.Character

  @impl DevJoy.Character
  def get_character_data(:john_doe), do: [smart: true]
end
