defmodule DevJoy.NestedTranslateTest do
  use ExUnit.Case, async: true

  alias DevJoy.Character
  alias DevJoy.Fixtures.SceneWithGettext
  alias DevJoy.Scene.Dialog
  alias DevJoy.Scene.Part

  character = %Character{
    avatar: "example.com/john_doe.ico",
    full_name: "John Doe",
    id: :john_doe,
    data: [smart: true]
  }

  @dialog_part_en %Part{
    data: [%Dialog{character: character, content: "Example dialog", side: :left}],
    name: :dialog,
    page_title: "Dialog box",
    scene: SceneWithGettext
  }

  @dialog_part_pl %Part{
    data: [%Dialog{character: character, content: "Przykładowy dialog", side: :left}],
    name: :dialog,
    page_title: "Okno dialogowe",
    scene: SceneWithGettext
  }

  test "nested_translate/2" do
    assert @dialog_part_en == SceneWithGettext.get_part(:dialog)
    assert @dialog_part_en == nested_translate(@dialog_part_en)
    Gettext.put_locale("pl")
    @dialog_part_pl = SceneWithGettext.get_part(:dialog)
    assert @dialog_part_pl == nested_translate(@dialog_part_en)
    assert @dialog_part_pl == nested_translate(@dialog_part_pl)
  end

  defp nested_translate(part) do
    DevJoy.Gettext.nested_translate(part, DevJoy.Fixtures.Gettext)
  end
end
