defmodule DevJoy.ForumDataTest do
  use ExUnit.Case, async: true

  alias DevJoy.Character.Cache
  alias DevJoy.ForumData

  avatar = "elixirforum.com/user_avatar/elixirforum.com/elixirforum/48/169_2.png"
  @character [avatar: avatar, full_name: "Elixir Forum"]

  describe "get_character/1" do
    test "stores character in cache" do
      refute Cache.get(:elixirforum)
      assert @character == ForumData.get_character(:elixirforum)
      assert @character == Cache.get(:elixirforum)
    end
  end
end
