defmodule DevJoy.Scene.AssetTest do
  use ExUnit.Case, async: true

  alias DevJoy.Fixtures.Endpoint
  alias DevJoy.Scene.Asset

  @path "sample/path.extension"
  @asset %Asset{path: @path}

  test "priv_path/2" do
    priv_dir = :code.priv_dir(:dev_joy)
    assert List.to_string(priv_dir) <> "/" <> @path == Asset.priv_path(@asset, :dev_joy)
  end

  describe "static_path/2" do
    test "base_path" do
      assert "assets/" <> @path == Asset.static_path(@asset, "assets")
    end

    test "endpoint" do
      assert "/assets/" <> @path == Asset.static_path(@asset, Endpoint)
    end

    test "nil" do
      assert @path == Asset.static_path(@asset)
    end

    test "uri" do
      uri = URI.parse("https://example.com/assets")
      assert "/assets/" <> @path == Asset.static_path(@asset, uri)
    end
  end
end
