defmodule DevJoy.AttributeTest do
  use ExUnit.Case, async: true

  alias DevJoy.Attribute

  require Attribute

  defmacrop in_module(block) do
    quote do
      defmodule(Temp, unquote(block))
      purge(Temp)
    end
  end

  defp purge(module) do
    :code.purge(module)
    :code.delete(module)
  end

  describe "delete" do
    test "delete/1" do
      in_module do
        Attribute.put(:attr_name, :value)
        assert Module.has_attribute?(__MODULE__, :__dev_joy_attr_name__)
        assert @__dev_joy_attr_name__ == Attribute.delete(:attr_name)
        refute Module.has_attribute?(__MODULE__, :__dev_joy_attr_name__)
      end
    end

    test "delete/2" do
      in_module do
        Attribute.put(Temp, :attr_name, :value)
        assert Module.has_attribute?(Temp, :__dev_joy_attr_name__)
        assert @__dev_joy_attr_name__ == Attribute.delete(Temp, :attr_name)
        refute Module.has_attribute?(Temp, :__dev_joy_attr_name__)
      end
    end
  end

  describe "delete_and_reverse" do
    test "delete_and_reverse/1" do
      in_module do
        Attribute.put(:attr_name, [:first_value])
        Attribute.update(:attr_name, &[:second_value | &1])
        assert @__dev_joy_attr_name__ == ~w[second_value first_value]a
        assert Module.has_attribute?(__MODULE__, :__dev_joy_attr_name__)
        assert Enum.reverse(@__dev_joy_attr_name__) == Attribute.delete_and_reverse(:attr_name)
        refute Module.has_attribute?(__MODULE__, :__dev_joy_attr_name__)
      end
    end

    test "delete_and_reverse/2" do
      in_module do
        Attribute.put(Temp, :attr_name, [:first_value])
        Attribute.update(Temp, :attr_name, &[:second_value | &1])
        assert @__dev_joy_attr_name__ == ~w[second_value first_value]a
        assert Module.has_attribute?(Temp, :__dev_joy_attr_name__)

        assert Enum.reverse(@__dev_joy_attr_name__) ==
                 Attribute.delete_and_reverse(Temp, :attr_name)

        refute Module.has_attribute?(Temp, :__dev_joy_attr_name__)
      end
    end
  end

  describe "get" do
    test "get/1" do
      in_module do
        @__dev_joy_attr_name__ :value
        assert @__dev_joy_attr_name__ == Attribute.get(:attr_name)
      end
    end

    test "get/2" do
      in_module do
        @__dev_joy_attr_name__ :value
        assert @__dev_joy_attr_name__ == Attribute.get(Temp, :attr_name)
      end
    end
  end

  describe "has?" do
    test "has?/1" do
      in_module do
        refute Attribute.has?(:attr_name)
        Attribute.put(:attr_name, :value)
        assert Attribute.has?(:attr_name)
      end
    end

    test "has?/2" do
      in_module do
        refute Attribute.has?(Temp, :attr_name)
        Attribute.put(Temp, :attr_name, :value)
        assert Attribute.has?(Temp, :attr_name)
      end
    end
  end

  describe "put" do
    test "put/2" do
      in_module do
        Attribute.put(:attr_name, :value)
        assert @__dev_joy_attr_name__ == :value
      end
    end

    test "put/3" do
      in_module do
        Attribute.put(Temp, :attr_name, :value)
        assert @__dev_joy_attr_name__ == :value
      end
    end
  end

  describe "update" do
    test "update/2" do
      in_module do
        Attribute.put(:attr_name, 5)
        Attribute.update(:attr_name, &(&1 * &1))
        assert @__dev_joy_attr_name__ == 25
      end
    end

    test "update/3" do
      in_module do
        Attribute.put(Temp, :attr_name, 5)
        Attribute.update(Temp, :attr_name, &(&1 * &1))
        assert @__dev_joy_attr_name__ == 25
      end
    end
  end
end
