defmodule DevJoy.CharacterTest do
  use ExUnit.Case, async: true

  alias DevJoy.Character
  alias DevJoy.Fixtures.DataModule
  alias DevJoy.Fixtures.ExtraModule

  @character [id: :john_doe, avatar: "example.com/john_doe.ico", full_name: "John Doe"]

  describe "get/1" do
    test "with extra module" do
      Application.put_env(:dev_joy, Character, data_module: DataModule, extra_module: ExtraModule)
      assert Keyword.put(@character, :data, smart: true) == Character.get_fields!(:john_doe)
    end

    test "without extra module" do
      Application.put_env(:dev_joy, Character, data_module: DataModule)
      assert @character == Character.get_fields!(:john_doe)
    end
  end
end
