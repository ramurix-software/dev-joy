defmodule DevJoy.StageTest do
  use ExUnit.Case, async: true

  alias DevJoy.Character
  alias DevJoy.Fixtures.Scene
  alias DevJoy.Scene.Asset
  alias DevJoy.Scene.Challenge
  alias DevJoy.Scene.Chapter
  alias DevJoy.Scene.Choice
  alias DevJoy.Scene.Dialog
  alias DevJoy.Scene.Menu
  alias DevJoy.Scene.Part

  character = %Character{
    avatar: "example.com/john_doe.ico",
    full_name: "John Doe",
    id: :john_doe,
    data: [smart: true]
  }

  @asset_part %Part{
    data: [%Asset{data: [some: :data], path: "/path/to/asset"}],
    name: :asset,
    page_title: "Asset",
    scene: Scene
  }

  @challenge_part %Part{
    data: [%Challenge{data: [some: :data], type: :challenge_type}],
    name: :challenge,
    page_title: "Challenge",
    scene: Scene
  }

  @chapter_part %Part{
    data: [%Chapter{description: "Chapter description", index: 1, title: "Chapter title"}],
    name: :chapter,
    page_title: "Chapter",
    scene: Scene
  }

  @dialog_part %Part{
    data: [%Dialog{character: character, content: "Example dialog", side: :left}],
    name: :dialog,
    page_title: "Dialog box",
    scene: Scene
  }

  @empty_part %Part{data: [], name: :empty, page_title: "Empty", scene: Scene}

  @menu %Menu{
    choices: [%{content: "Menu item content"}],
    title: "Menu title"
  }

  @question %Dialog{
    character: character,
    choices: [%{content: "Example answer"}],
    content: "Example question",
    side: :left
  }

  describe "get_part/0" do
    assert Scene.get_part(:main) == Scene.get_part()
  end

  describe "get_part/1" do
    test "asset" do
      assert @asset_part == Scene.get_part(:asset)
    end

    test "challenge" do
      assert @challenge_part == Scene.get_part(:challenge)
    end

    test "chapter" do
      assert @chapter_part == Scene.get_part(:chapter)
    end

    test "condition" do
      part = Scene.get_part(:condition)
      assert %Part{data: [data], name: :condition, scene: Scene} = part
      assert "Condition" == part.page_title
      assert_goto(data, @empty_part)
    end

    test "dialog" do
      assert @dialog_part == Scene.get_part(:dialog)
    end

    test "empty" do
      assert @empty_part == Scene.get_part(:empty)
    end

    test "goto" do
      main = Scene.get_part(:main)
      assert "Main" == main.page_title
      assert %Part{data: [%Dialog{choices: choices}]} = main
      assert Enum.all?(choices, &(&1.action.() == main))
      part = Scene.get_part(:goto)
      assert %Part{data: [goto], name: :goto, scene: Scene} = part
      assert "Go to" == part.page_title
      assert_goto(goto, main)
    end

    test "menu" do
      assert_part_with_choices(@menu, :menu, Menu, "Menu")
    end

    test "question" do
      assert_part_with_choices(@question, :question, Dialog, "Question")
    end
  end

  @spec assert_part_with_choices(struct, Part.name(), module, String.t()) :: Part.t()
  defp assert_part_with_choices(expected_data, name, module, page_title) do
    part = Scene.get_part(name)
    assert page_title == part.page_title
    assert %Part{data: [data], name: ^name, scene: Scene} = part
    assert is_struct(data, module)
    %{choices: [%{content: expected_content}]} = expected_data
    assert [%Choice{action: action, content: ^expected_content}] = data.choices
    assert ^expected_data = %{data | choices: [%{content: expected_content}]}
    assert_goto(action, @empty_part)
  end

  @spec assert_goto((-> Part.t()), Part.t()) :: Part.t()
  defp assert_goto(action, part) do
    assert is_function(action, 0)
    assert part == action.()
  end
end
