defmodule DevJoy.ExtractTest do
  use ExUnit.Case, async: false

  alias Mix.Tasks.Compile

  @pot """
  ## This file is a PO Template file.
  ##
  ## "msgid"s here are often extracted from source code.
  ## Add new messages manually only if they're dynamic
  ## messages that can't be statically extracted.
  ##
  ## Run "mix gettext.extract" to bring this file up to
  ## date. Leave "msgstr"s empty as changing them here has no
  ## effect: edit them in PO (.po) files instead.
  #
  msgid ""
  msgstr ""

  #: test/support/fixtures/extract.ex:12
  #, elixir-autogen, elixir-format
  msgctxt "dialog"
  msgid "Dialog box"
  msgstr ""

  #: test/support/fixtures/extract.ex:13
  #, elixir-autogen, elixir-format
  msgctxt "dialog"
  msgid "Example dialog"
  msgstr ""
  """

  @gettext_fixtures "tmp/extract-test"

  @tag :recompile
  test "extract/6" do
    refute File.exists?(@gettext_fixtures)
    ExUnit.CaptureIO.capture_io(&recompile/0)
    assert File.exists?(@gettext_fixtures)
    path = Path.join(@gettext_fixtures, "DevJoy.Fixtures.Extract.Scene.pot")
    assert File.exists?(path)
    assert File.read!(path) == @pot
    File.rm_rf(@gettext_fixtures)
  end

  defp recompile do
    Code.put_compiler_option(:ignore_module_conflict, true)
    Mix.Task.run("gettext.extract")
    # Reload config after gettext recompile
    Mix.Task.run("loadconfig", ["config/config.exs"])
    Compile.Elixir.clean()
    Enum.each(Compile.Elixir.manifests(), &File.rm/1)
    # If "compile" was never called, the reenabling is a no-op and
    # "compile.elixir" is a no-op as well (because it wasn't reenabled after
    # running "compile"). If "compile" was already called, then running
    # "compile" is a no-op and running "compile.elixir" will work because we
    # manually reenabled it.
    Mix.Task.reenable("compile.elixir")
    Mix.Task.run("compile")
    Mix.Task.run("compile.elixir", ["--force"])
    Code.put_compiler_option(:ignore_module_conflict, false)
  end
end
