defmodule DevJoy.Character.CacheTest do
  use ExUnit.Case, async: true

  alias DevJoy.Character.Cache

  @character [avatar: "path/to/avatar.png", full_name: "Elixir Forum"]

  test "caches character" do
    refute Cache.get(:cache_test)
    Cache.put(:cache_test, @character)
    assert @character == Cache.get(:cache_test)
  end
end
