defmodule DevJoy.SceneWithGettextTest do
  use ExUnit.Case, async: true

  alias DevJoy.Character
  alias DevJoy.Fixtures.SceneWithGettext
  alias DevJoy.Scene.Dialog
  alias DevJoy.Scene.Menu
  alias DevJoy.Scene.Part

  character = %Character{
    avatar: "example.com/john_doe.ico",
    full_name: "John Doe",
    id: :john_doe,
    data: [smart: true]
  }

  @dialog_part %Part{
    data: [%Dialog{character: character, content: "Przykładowy dialog", side: :left}],
    name: :dialog,
    page_title: "Okno dialogowe",
    scene: SceneWithGettext
  }

  @menu %Menu{
    choices: [%{content: "Zawartość elementu menu"}],
    title: "Tytuł menu"
  }

  @question %Dialog{
    character: character,
    choices: [%{content: "Przykładowa odpowiedź"}],
    content: "Przykładowe pytanie",
    side: :left
  }

  describe "get_part/1" do
    test "asset" do
      Gettext.put_locale("pl")
      assert "Zasób" == SceneWithGettext.get_part(:asset).page_title
    end

    test "challenge" do
      Gettext.put_locale("pl")
      assert "Wyzwanie" == SceneWithGettext.get_part(:challenge).page_title
    end

    test "chapter" do
      Gettext.put_locale("pl")
      %{data: [chapter]} = part = SceneWithGettext.get_part(:chapter)
      assert "Rozdział" == part.page_title
      assert "Opis rozdziału" == chapter.description
      assert "Tytuł rozdziału" == chapter.title
    end

    test "dialog" do
      Gettext.put_locale("pl")
      assert @dialog_part == SceneWithGettext.get_part(:dialog)
    end

    test "empty" do
      Gettext.put_locale("pl")
      assert "Pusty" == SceneWithGettext.get_part(:empty).page_title
    end

    test "main" do
      Gettext.put_locale("pl")
      assert "Główny" == SceneWithGettext.get_part(:main).page_title
    end

    test "menu" do
      Gettext.put_locale("pl")
      assert_part_with_choices(@menu, :menu)
    end

    test "question" do
      Gettext.put_locale("pl")
      assert_part_with_choices(@question, :question)
    end
  end

  @spec assert_part_with_choices(struct, Part.name()) :: Dialog.t() | Menu.t()
  defp assert_part_with_choices(expected_data, name) do
    assert %Part{data: [data], name: ^name, scene: SceneWithGettext} =
             SceneWithGettext.get_part(name)

    %{choices: [%{content: expected_content}]} = expected_data
    assert [%{content: ^expected_content}] = data.choices
    assert ^expected_data = %{data | choices: [%{content: expected_content}]}
  end
end
