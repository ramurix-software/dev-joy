defmodule DevJoyTest do
  use ExUnit.Case, async: true

  @ast quote(do: fn -> :ok end)

  describe "escape_all_but_anonymous_function_ast/1" do
    test "func" do
      assert @ast == DevJoy.escape_all_but_anonymous_function_ast(@ast)
    end

    test "list" do
      assert [@ast] == DevJoy.escape_all_but_anonymous_function_ast([@ast])
    end

    test "map" do
      assert {:%{}, [], [key: @ast]} == DevJoy.escape_all_but_anonymous_function_ast(%{key: @ast})
    end

    test "nested" do
      expected = {:%{}, [], [key: [{:%{}, [], [other_key: @ast]}]]}
      assert expected == DevJoy.escape_all_but_anonymous_function_ast(%{key: [%{other_key: @ast}]})
    end
  end
end
