defmodule DevJoy.Character.Cache do
  @moduledoc false

  use GenServer

  alias DevJoy.Character
  alias DevJoy.Scene

  # Client

  @spec start_link(GenServer.options()) :: GenServer.on_start()
  def start_link(_opts \\ []), do: GenServer.start_link(__MODULE__, %{}, name: __MODULE__)

  @spec get(Character.id()) :: Scene.struct_fields() | nil
  def get(id), do: GenServer.call(__MODULE__, {:get, id})

  @spec put(Character.id(), Scene.struct_fields()) :: :ok
  def put(id, data), do: GenServer.cast(__MODULE__, {:put, id, data})

  # Server (callbacks)

  @impl GenServer
  @spec init(map) :: {:ok, map}
  def init(data), do: {:ok, data}

  @impl GenServer
  @spec handle_call({:get, id}, GenServer.from(), %{id => data}) :: {:reply, data, %{}}
        when data: Scene.struct_fields(), id: Character.id()
  def handle_call({:get, id}, _from, state) when is_map_key(state, id) do
    {:reply, state[id], state}
  end

  @spec handle_call({:get, Character.id()}, GenServer.from(), map) :: {:noreply, nil, %{}}
  def handle_call({:get, _id}, _from, state), do: {:reply, nil, state}

  @impl GenServer
  @spec handle_cast({:put, id, data}, map) :: {:noreply, %{id => data}}
        when data: Scene.struct_fields(), id: Character.id()
  def handle_cast({:put, id, data}, state), do: {:noreply, Map.put(state, id, data)}
end
