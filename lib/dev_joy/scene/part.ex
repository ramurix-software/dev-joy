defmodule DevJoy.Scene.Part do
  @moduledoc """
  A part may be used to split a long scene or adding an alternative scenario.

  ## Example

      defmodule MyApp.SceneName do
        use DevJoy.Scene

        part :part_name do
          # elements DSL goes here
        end
      end
  """

  @behaviour DevJoy.Gettext

  alias DevJoy.Gettext
  alias DevJoy.Scene.Asset
  alias DevJoy.Scene.Challenge
  alias DevJoy.Scene.Chapter
  alias DevJoy.Scene.Choice
  alias DevJoy.Scene.Dialog
  alias DevJoy.Scene.Menu

  defstruct [:name, :page_title, :scene, data: []]

  @typedoc "Part struct"
  @type t :: %__MODULE__{
          data: [Asset.t() | Challenge.t() | Chapter.t() | Choice.action() | Dialog.t() | Menu.t()],
          name: name,
          page_title: page_title,
          scene: scene
        }

  @typedoc "Name used to navigate between parts"
  @type name :: atom

  @typedoc "Page title"
  @type page_title :: String.t() | nil

  @typedoc "Scene module in which part has been defined"
  @type scene :: module

  @impl Gettext
  @spec fields_to_translate :: Gettext.fields_to_translate()
  def fields_to_translate, do: [:page_title]
end
