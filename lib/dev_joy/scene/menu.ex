defmodule DevJoy.Scene.Menu do
  @moduledoc """
  Menu visually represented as a titled list of buttons allows to navigate between scenes and their parts.

  ## Example

      defmodule MyApp.SceneWithMenu do
        use DevJoy.Scene

        part :menu do
          menu "Menu title" do
            choice "Menu item content", goto(:part_name)
          end
        end
      end
  """

  @behaviour DevJoy.Gettext

  alias DevJoy.Gettext
  alias DevJoy.Scene.Choice

  defstruct ~w[choices title]a

  @typedoc "Menu struct"
  @type t :: %__MODULE__{
          choices: [Choice.t(String.t())],
          title: title
        }

  @typedoc "Title"
  @type title :: String.t()

  @impl Gettext
  @spec fields_to_translate :: Gettext.fields_to_translate()
  def fields_to_translate, do: [:title]
end
