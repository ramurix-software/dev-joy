defmodule DevJoy.Scene.Challenge do
  @moduledoc """
  Challenge is a polimorfic custom content specific for your game.
  It could be a mini-game or other custom action user have complete to progress in game.

  ## Example

      defmodule MyApp.SceneWithChallenge do
        use DevJoy.Scene

        part :challenge do
          challenge :challenge_type, some: :data
        end
      end
  """

  defstruct [:type, data: []]

  @typedoc "Challenge struct"
  @type t :: %__MODULE__{data: data, type: type}

  @typedoc "Additional data"
  @type data :: Keyword.t()

  @typedoc "Type used to identify challenge"
  @type type :: atom
end
