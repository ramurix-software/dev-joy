defmodule DevJoy.Scene.Dialog do
  @moduledoc """
  A dialog visually represented in dialog box contains a text to be displayed.
  Dialog have an optional choices which allows for example to answer a character's question.
  Dialog allows to display the character information on opposite side.
  This adds a bit of flexibility for UI design, so dialogs could be styled as messages in chat application.

  ## Example

      defmodule MyApp.SceneWithDialog do
        use DevJoy.Scene

        part :dialog do
          dialog :john_doe, "Example dialog"
        end
      end
  """

  @behaviour DevJoy.Gettext

  alias DevJoy.Character
  alias DevJoy.Gettext
  alias DevJoy.Scene.Choice

  defstruct [:character, :content, choices: [], side: :left]

  @typedoc "Dialog struct"
  @type t :: %__MODULE__{
          character: Character.t(),
          choices: [Choice.t(String.t())],
          content: content,
          side: side
        }

  @typedoc "Dialog box content"
  @type content :: String.t()

  @typedoc "Determines on which side of dialog box character should be placed"
  @type side :: :left | :right

  @impl Gettext
  @spec fields_to_translate :: Gettext.fields_to_translate()
  def fields_to_translate, do: [:content]
end
