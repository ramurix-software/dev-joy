defmodule DevJoy.Scene.Choice do
  @moduledoc """
  Choice visually presented as button gives user a possibility to apply an action.
  It could be a simple menu item as well as an answer for a character question.
  Returned data may be used to perform a further changes in UI.

  ## Example

      choice = Enum.at(menu_or_question.choices, choice_index)
      choice.action.()
  """

  @behaviour DevJoy.Gettext

  alias DevJoy.Gettext

  defstruct ~w[action content]a

  @typedoc """
  Choice struct

  The content could be a `t:String.t/0` or `t:atom/0` depending on use case.
  Atom values are not translated no matter if `gettext_module` option is used in scene.
  """
  @type t(content) :: %__MODULE__{action: action, content: content}

  @typedoc "Function to call"
  @type action :: (-> any)

  @typedoc "Content"
  @type content :: atom | String.t()

  @impl Gettext
  @spec fields_to_translate :: Gettext.fields_to_translate()
  def fields_to_translate, do: [:content]
end
