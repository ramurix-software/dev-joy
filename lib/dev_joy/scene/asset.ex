defmodule DevJoy.Scene.Asset do
  @moduledoc """
  Asset gives a way to display an image, play a sound or work with any other files.
  Data allows to configure how the asset should be used.
  For example the same image may be displayed as a background or inside an image preview popup.
  The sounds on the other side could be repeated.
  Other files or even directories could be used to show a gallery or open it in external application.

  ## Example

      defmodule MyApp.SceneWithAsset do
        use DevJoy.Scene

        part :asset do
          asset "/path/to/asset", some: :data
        end
      end
  """

  defstruct [:path, data: []]

  @typedoc "Asset struct"
  @type t :: %__MODULE__{data: data, path: path}

  @typedoc "Additional data"
  @type data :: Keyword.t()

  @typedoc "Relative path to file"
  @type path :: String.t()

  @doc "Returns absolute path for asset"
  @spec priv_path(t(), otp_app :: atom) :: String.t()
  def priv_path(%__MODULE__{path: path}, otp_app) do
    otp_app
    |> :code.priv_dir()
    |> Path.join(path)
  end

  @doc "Returns static path for asset"
  @spec static_path(t(), base_path | endpoint | uri | nil) :: String.t()
        when base_path: String.t(), endpoint: module, uri: URI.t()
  def static_path(asset, base_path_or_endpoint_or_uri \\ nil)

  def static_path(%__MODULE__{path: path}, nil), do: path

  def static_path(%__MODULE__{path: path}, base_path) when is_binary(base_path) do
    Path.join(base_path, path)
  end

  def static_path(%__MODULE__{path: path}, endpoint) when is_atom(endpoint) do
    endpoint.static_path(path)
  end

  def static_path(%__MODULE__{} = asset, %URI{path: base_path}) do
    static_path(asset, base_path)
  end
end
