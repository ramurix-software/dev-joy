defmodule DevJoy.Scene.Chapter do
  @moduledoc """
  A chapter presents a title and description of the next content part in the game.

  ## Example

      defmodule MyApp.SceneName do
        use DevJoy.Scene

        part :chapter do
          chapter 1, "First chapter", "Contains amazing stuff!"
        end
      end
  """

  @behaviour DevJoy.Gettext

  alias DevJoy.Gettext

  defstruct ~w[description index title]a

  @typedoc "Part struct"
  @type t :: %__MODULE__{
          description: description,
          index: index,
          title: title
        }

  @typedoc "Description"
  @type description :: String.t()

  @typedoc "Index"
  @type index :: non_neg_integer

  @typedoc "Title"
  @type title :: String.t()

  @impl Gettext
  @spec fields_to_translate :: Gettext.fields_to_translate()
  def fields_to_translate, do: ~w[description title]a
end
