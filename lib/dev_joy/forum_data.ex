defmodule DevJoy.ForumData do
  @moduledoc """
  An example character's data module which fetches forum data using username and Discourse API.
  It uses only publicly available data which does not requires any authentication.

  ## Configuration

  There are 2 simple steps.
  Firstly instruct a character configuration to use this module.
  Secondly configure this module to use your desired endpoint.

      config :dev_joy, DevJoy.Character, data_module: #{inspect(__MODULE__)}
      config :dev_joy, #{inspect(__MODULE__)}, endpoint: "https://meta.discourse.org"
  """

  @behaviour DevJoy.Character

  alias DevJoy.Character
  alias DevJoy.Character.Cache
  alias DevJoy.Scene

  @avatar_size 48

  @impl DevJoy.Character
  def get_character(id) do
    {:ok, _} = Application.ensure_all_started(:dev_joy)

    id
    |> Cache.get()
    |> then(&(&1 || fetch_forum_data(id)))
  end

  @spec fetch_forum_data(Character.id()) :: Scene.struct_fields()
  defp fetch_forum_data(id) do
    {:ok, _} = Application.ensure_all_started(:req)

    :dev_joy
    |> Application.get_env(__MODULE__, endpoint: "https://elixirforum.com")
    |> Keyword.fetch!(:endpoint)
    |> Path.join("u/#{id}.json")
    |> Req.get!()
    |> then(& &1.body["user"])
    |> then(&[avatar: avatar_from_template(&1["avatar_template"]), full_name: &1["name"]])
    |> tap(&Cache.put(id, &1))
  end

  @spec avatar_from_template(String.t()) :: Character.avatar()
  defp avatar_from_template(template) do
    template
    |> String.replace("{size}", Integer.to_string(@avatar_size))
    |> then(&Path.join("elixirforum.com", &1))
  end
end
