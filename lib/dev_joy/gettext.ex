defmodule DevJoy.Gettext do
  @moduledoc false

  alias DevJoy.Scene
  alias DevJoy.Scene.Part
  alias Gettext.Compiler
  alias Gettext.Extractor
  alias Macro.Env

  @callback fields_to_translate :: list(atom)

  @type domain :: module
  @type fields_to_translate :: list(atom)
  @type msgctxt :: atom

  @doc "Helper for extracting messages within DSL"
  @spec extract(
          Gettext.backend(),
          Part.scene(),
          Part.name(),
          Env.t(),
          Scene.struct_module(),
          Scene.struct_fields()
        ) :: :ok | nil
  def extract(backend, domain, msgctxt, caller, struct_module, fields) do
    if Extractor.extracting?() && function_exported?(struct_module, :fields_to_translate, 0) do
      for field <- struct_module.fields_to_translate() do
        spawn(fn -> extract(backend, domain, msgctxt, fields[field], caller) end)
      end
    end
  end

  @spec extract(
          Gettext.backend(),
          Part.scene(),
          Part.name(),
          Env.t(),
          String.t(),
          Scene.struct_module()
        ) :: :ok | nil
  defp extract(backend, domain, msgctxt, msgid, caller) do
    domain =
      domain
      |> inspect()
      |> Compiler.expand_to_binary("domain", backend, caller)

    msgctxt =
      msgctxt
      |> Atom.to_string()
      |> Compiler.expand_to_binary("msgctxt", backend, caller)

    msgid = Compiler.expand_to_binary(msgid, "msgid", backend, caller)
    extracted_comments = Compiler.get_and_flush_extracted_comments()
    Extractor.extract(caller, backend, domain, msgctxt, msgid, extracted_comments)
  end

  @doc "Traverses lists and structs searchin for a translatable fields an performs their translate"
  @spec nested_translate(Part.t(), Gettext.backend()) :: Part.t()
  def nested_translate(part, backend) do
    nested_translate(part, backend, inspect(part.scene), to_string(part.name))
  end

  @spec nested_translate(struct, Gettext.backend(), domain, msgctxt) :: struct
  defp nested_translate(%module{} = struct, backend, domain, msgctxt) do
    fields_to_translate =
      if function_exported?(module, :fields_to_translate, 0) do
        module.fields_to_translate()
      else
        []
      end

    struct
    |> Map.to_list()
    |> Enum.reduce(struct, fn {key, value}, acc ->
      value =
        if key in fields_to_translate do
          acc
          |> Map.fetch!(key)
          |> then(&Gettext.dpgettext(backend, domain, msgctxt, &1))
        else
          nested_translate(value, backend, domain, msgctxt)
        end

      Map.put(acc, key, value)
    end)
  end

  @spec nested_translate(list, Gettext.backend(), domain, msgctxt) :: list
  defp nested_translate(list, backend, domain, msgctxt) when is_list(list) do
    Enum.map(list, &nested_translate(&1, backend, domain, msgctxt))
  end

  @spec nested_translate(any, Gettext.backend(), domain, msgctxt) :: any
  defp nested_translate(data, _backend, _domain, _msgctxt), do: data
end
