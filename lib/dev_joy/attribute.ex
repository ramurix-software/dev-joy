defmodule DevJoy.Attribute do
  @moduledoc false

  # A simple set of macros which allows to nicely prefix the name of module attributes.
  # This allows to use even simplest naming without a worry that it would cause a problem in existing projects.
  # It's solved using macros in order to provide a short private API call without passing __MODULE__ in each call.

  @doc "Deletes the attribute returning its value"
  @spec delete(module(), atom()) :: Macro.t()
  defmacro delete(module \\ __CALLER__.module, name) do
    quote bind_quoted: [key: name_to_key(name), module: module] do
      Module.delete_attribute(module, key)
    end
  end

  @doc "Deletes the attribute returning its value in reverse order"
  @spec delete_and_reverse(module(), atom()) :: Macro.t()
  defmacro delete_and_reverse(module \\ __CALLER__.module, name) do
    quote bind_quoted: [key: name_to_key(name), module: module] do
      module
      |> Module.delete_attribute(key)
      |> Enum.reverse()
    end
  end

  @doc "Returns an attribute value"
  @spec get(module(), atom()) :: Macro.t()
  defmacro get(module \\ __CALLER__.module, name) do
    quote bind_quoted: [key: name_to_key(name), module: module] do
      Module.get_attribute(module, key)
    end
  end

  @doc "Returns true if module has attribute and false otherwise"
  @spec has?(module(), atom()) :: Macro.t()
  defmacro has?(module \\ __CALLER__.module, name) do
    quote bind_quoted: [key: name_to_key(name), module: module] do
      Module.has_attribute?(module, key)
    end
  end

  @doc "Puts an attribute value"
  @spec put(module(), atom(), term()) :: Macro.t()
  defmacro put(module \\ __CALLER__.module, name, value) do
    quote bind_quoted: [key: name_to_key(name), module: module, value: value] do
      Module.put_attribute(module, key, value)
    end
  end

  @doc "Updates an attribute value with the specified function"
  @spec update(module(), atom(), term()) :: Macro.t()
  defmacro update(module \\ __CALLER__.module, name, update_func) do
    quote bind_quoted: [key: name_to_key(name), module: module, update_func: update_func] do
      module
      |> Module.get_attribute(key)
      |> update_func.()
      |> then(&Module.put_attribute(module, key, &1))
    end
  end

  @spec name_to_key(atom) :: atom
  # credo:disable-for-next-line Credo.Check.Warning.UnsafeToAtom
  defp name_to_key(name), do: :"__dev_joy_#{name}__"
end
