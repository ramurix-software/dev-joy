defmodule DevJoy.Application do
  @moduledoc false

  use Application

  alias DevJoy.Character.Cache

  @impl Application
  def start(_type, _args) do
    Supervisor.start_link([Cache], name: DevJoy.Supervisor, strategy: :one_for_one)
  end
end
