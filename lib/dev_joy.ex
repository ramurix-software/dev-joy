defmodule DevJoy do
  @moduledoc false

  @doc """
  A small and simple trick to escape everything but anonymous function AST
  This is useful when we need a quoted anonymous function to be passed to function with `unquote/1` call
  """
  @spec escape_all_but_anonymous_function_ast(map) :: Macro.output()
  def escape_all_but_anonymous_function_ast(map) when is_map(map) do
    list = Map.to_list(map)

    %{}
    |> Macro.escape()
    |> put_elem(2, escape_all_but_anonymous_function_ast(list))
  end

  @spec escape_all_but_anonymous_function_ast({any, any}) :: Macro.output()
  def escape_all_but_anonymous_function_ast({key, value}) do
    {Macro.escape(key), escape_all_but_anonymous_function_ast(value)}
  end

  @spec escape_all_but_anonymous_function_ast(list) :: Macro.output()
  def escape_all_but_anonymous_function_ast(list) when is_list(list) do
    Enum.map(list, &escape_all_but_anonymous_function_ast/1)
  end

  @spec escape_all_but_anonymous_function_ast({:fn, [], [{:->, [], Macro.input()}]}) ::
          Macro.output()
  def escape_all_but_anonymous_function_ast({:fn, [], [{:->, [], [[], _block]}]} = ast), do: ast

  @spec escape_all_but_anonymous_function_ast(any()) :: Macro.output()
  def escape_all_but_anonymous_function_ast(data), do: data
end
