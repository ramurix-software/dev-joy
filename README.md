# DevJoy

A package to simplify creation of Visual Novel games.

[![Hex.pm License](https://img.shields.io/hexpm/l/dev_joy?color=purple)](https://www.apache.org/licenses/LICENSE-2.0)
[![Hex.pm Version](https://img.shields.io/hexpm/v/dev_joy?color=blue)](https://hex.pm/packages/dev_joy)
[![Static Badge](https://img.shields.io/badge/docs-limegreen?label=DevJoy&labelColor=gray)](https://hexdocs.pm/dev_joy)

## Installation

```elixir
def deps do
  [
    {:dev_joy, "~> 1.1"}
  ]
end
```

1. Create a new `phoenix` or `scenic` project or just use existing one
2. Add `dev_joy` to your dependencies
3. Add English content using DSL
4. Add assets to your game
5. Fetch your first scene
6. Draw your game following output of `current_scene_module.get_part()` or `current_scene_module.get_part(part_name)`
7. Optionally translate content using gettext (call `mix gettext.extract`).

Except drawing you only have to provide a content to the game. To "draw" your game all you need to do is to convert structs into `phoenix` templates or `scenic` graphs and style them respectively.

## Example scenes

### Main menu

```elixir
defmodule MyApp.Scenes.Main do
  use DevJoy.Scene

  alias MyApp.Scenes.FirstStage
  alias MyApp.Scenes.SecondStage
  alias MyApp.Scenes.ThirdStage

  part :main do
    menu "Welcome in MyApp!" do
      asset "/assets/main/background1.jpg", size: :stretch_to_fit, type: :backgound
      asset "/assets/main/background1.mp3", loop: true, type: :background
      choice "First stage", goto(FirstStage, :main)
      choice "Second stage", goto(SecondStage, :main)
      choice "Third stage", goto(ThirdStage, :main)
      choice "Settings", goto(:settings)
      choice "Credits", goto(:credits)
    end
  end

  part :credits do
    dialog :admin, "Assets: me (spend lots of time on search for free image and sounds)"
    dialog :admin, "Scenario: me (generated it by AI)"
    dialog :admin, "Translations: me (I'm just young kid, so nobody would notice that I have used some random free translator)"
    dialog :admin, "Game fun: … (on TODO list already!)"
    dialog :admin, "Aren't I the best? >^·^<"
    goto()
  end

  part :settings do
    dialog :admin, "Still working on it …"
    dialog :admin, "Are you stil here ?!?!"
    dialog :admin, "Oh, please! There are no achievements in this game!"
    dialog :admin, "Seriously …"
    dialog :admin, "Oh, right! I haven't instructed the game to navigate you back to main menu!"
    goto()
  end
end
```

### Your game's first stage

```elixir
defmodule MyApp.Scenes.FirstStage do
  use DevJoy.Scene

  alias MyApp.Scenes.Main
  alias MyApp.Scenes.SecondStage

  part :main do
    asset "/assets/first-stage/background1.jpg", size: :stretch_to_fit, type: :backgound
    asset "/assets/first-stage/background1.mp3", loop: false, type: :foreground
    dialog :admin, "Lorem ipsum …"
    challenge :input_next_words, count: 3, for: "Lorem ipsum"
    asset "/assets/first-stage/admin-bored.jpg", duration: "0.5s", type: :preview

    question :admin, "Aren't you tired already?" do
      choice "Yes! I'm done!", goto(Main, :main)
      choice "Why? Isn't that just a start?", goto(SecondStage, :main)
    end
  end
end
```
