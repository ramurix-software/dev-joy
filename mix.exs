defmodule DevJoy.MixProject do
  use Mix.Project

  @name :dev_joy
  @version "1.1.0"

  @groups_for_modules [
    "Structs for DSL": [
      DevJoy.Scene.Asset,
      DevJoy.Scene.Element.Challenge,
      DevJoy.Scene.Element.Choice,
      DevJoy.Scene.Element.Dialog,
      DevJoy.Scene.Element.Menu,
      DevJoy.Scene.Part
    ]
  ]

  @source_url "https://gitlab.com/ramurix-software/dev-joy"

  @deps [
    # Code checking
    {:credo, "~> 1.7", only: :dev, optional: true, runtime: false},
    {:dialyxir, "~> 1.4", only: :dev, optional: true, runtime: false},
    # Documentation
    {:ex_doc, "~> 0.31", only: :dev, optional: true, runtime: false},
    # Tests
    {:excoveralls, "~> 0.18", only: :test, optional: true},
    # optional support
    {:gettext, "~> 0.24"},
    # DevJoy.ForumData
    req: "~> 0.4"
  ]

  def application, do: [mod: {DevJoy.Application, []}]

  def cli, do: [preferred_envs: [coveralls: :test, "coveralls.html": :test]]

  def project do
    [
      app: @name,
      deps: @deps,
      description: "A package to simplify creation of Visual Novel games.",
      docs: [
        extras: ["README.md": [filename: "#{@name}"]],
        groups_for_modules: @groups_for_modules,
        main: "#{@name}",
        source_ref: "v#{@version}",
        source_url: @source_url
      ],
      elixir: "~> 1.16",
      elixirc_paths: elixirc_paths(Mix.env()),
      name: "DevJoy",
      package: [
        files: ~w(.formatter.exs lib LICENSE mix.exs README.md),
        licenses: ["Apache-2.0"],
        links: %{"GitLab" => @source_url},
        maintainers: ["Tomasz Sulima"]
      ],
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      version: @version
    ]
  end

  defp elixirc_paths(:test), do: ~w[lib test/support]
  defp elixirc_paths(_), do: ~w[lib]
end
