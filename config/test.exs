import Config

alias DevJoy.Character
alias DevJoy.Fixtures.DataModule
alias DevJoy.Fixtures.ExtraModule

config :dev_joy, Character, data_module: DataModule, extra_module: ExtraModule

config :logger, level: :warning
