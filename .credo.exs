alias Credo.Check.Consistency
alias Credo.Check.Design
alias Credo.Check.Readability
alias Credo.Check.Refactor
alias Credo.Check.Warning

%{
  configs: [
    %{
      color: true,
      files: %{excluded: [".elixir_ls", "_build", "deps", "priv"], included: ["lib", "test"]},
      name: "default",
      parse_timeout: 5000,
      plugins: [],
      requires: [],
      strict: true,
      checks: %{
        disabled: [
          # Not compatible with Elixir ~> 1.16
          {Warning.LazyLogging, []}
        ],
        enabled: [
          {Consistency.ExceptionNames, []},
          {Consistency.LineEndings, []},
          {Consistency.MultiAliasImportRequireUse, []},
          {Consistency.ParameterPatternMatching, []},
          {Consistency.SpaceAroundOperators, []},
          {Consistency.SpaceInParentheses, []},
          {Consistency.TabsOrSpaces, []},
          {Consistency.UnusedVariableNames, []},
          {Design.AliasUsage, [if_called_more_often_than: 0, if_nested_deeper_than: 2, priority: :low]},
          {Design.DuplicatedCode, []},
          {Design.SkipTestWithoutComment, []},
          {Design.TagFIXME, []},
          {Design.TagTODO, [exit_status: 2]},
          {Readability.AliasAs, []},
          {Readability.AliasOrder, []},
          {Readability.BlockPipe, []},
          {Readability.FunctionNames, []},
          {Readability.ImplTrue, []},
          {Readability.LargeNumbers, []},
          {Readability.MaxLineLength, [priority: :low, max_length: 120]},
          {Readability.ModuleAttributeNames, []},
          {Readability.ModuleDoc, []},
          {Readability.ModuleNames, []},
          {Readability.MultiAlias, []},
          {Readability.NestedFunctionCalls, []},
          {Readability.OneArityFunctionInPipe, []},
          {Readability.OnePipePerLine, []},
          {Readability.ParenthesesInCondition, []},
          {Readability.ParenthesesOnZeroArityDefs, []},
          {Readability.PipeIntoAnonymousFunctions, []},
          {Readability.PredicateFunctionNames, []},
          {Readability.PreferImplicitTry, []},
          {Readability.RedundantBlankLines, []},
          {Readability.Semicolons, []},
          {Readability.SeparateAliasRequire, []},
          {Readability.SingleFunctionToBlockPipe, []},
          {Readability.SinglePipe, []},
          {Readability.SpaceAfterCommas, []},
          {Readability.Specs, []},
          {Readability.StrictModuleLayout, []},
          {Readability.StringSigils, []},
          {Readability.TrailingBlankLine, []},
          {Readability.TrailingWhiteSpace, []},
          {Readability.UnnecessaryAliasExpansion, []},
          {Readability.VariableNames, []},
          {Readability.WithCustomTaggedTuple, []},
          {Readability.WithSingleClause, []},
          {Refactor.ABCSize, []},
          {Refactor.AppendSingleItem, []},
          {Refactor.Apply, []},
          {Refactor.CondStatements, []},
          {Refactor.CyclomaticComplexity, []},
          {Refactor.DoubleBooleanNegation, []},
          {Refactor.FilterCount, []},
          {Refactor.FilterFilter, []},
          {Refactor.FilterReject, []},
          {Refactor.FunctionArity, []},
          {Refactor.IoPuts, []},
          {Refactor.LongQuoteBlocks, []},
          {Refactor.MapJoin, []},
          {Refactor.MapMap, []},
          {Refactor.MatchInCondition, []},
          {Refactor.ModuleDependencies, []},
          {Refactor.NegatedConditionsInUnless, []},
          {Refactor.NegatedConditionsWithElse, []},
          {Refactor.NegatedIsNil, []},
          {Refactor.Nesting, []},
          {Refactor.PassAsyncInTestCases, []},
          {Refactor.PipeChainStart, []},
          {Refactor.RedundantWithClauseResult, []},
          {Refactor.RejectFilter, []},
          {Refactor.RejectReject, []},
          {Refactor.UnlessWithElse, []},
          {Refactor.UtcNowTruncate, []},
          {Refactor.VariableRebinding, []},
          {Refactor.WithClauses, []},
          {Warning.ApplicationConfigInModuleAttribute, []},
          {Warning.BoolOperationOnSameValues, []},
          {Warning.Dbg, []},
          {Warning.ExpensiveEmptyEnumCheck, []},
          {Warning.IExPry, []},
          {Warning.IoInspect, []},
          # {Warning.LazyLogging, []},
          {Warning.LeakyEnvironment, []},
          {Warning.MapGetUnsafePass, []},
          {Warning.MissedMetadataKeyInLoggerConfig, []},
          {Warning.MixEnv, []},
          {Warning.OperationOnSameValues, []},
          {Warning.OperationWithConstantResult, []},
          {Warning.RaiseInsideRescue, []},
          {Warning.SpecWithStruct, []},
          {Warning.UnsafeExec, []},
          {Warning.UnsafeToAtom, []},
          {Warning.UnusedEnumOperation, []},
          {Warning.UnusedFileOperation, []},
          {Warning.UnusedKeywordOperation, []},
          {Warning.UnusedListOperation, []},
          {Warning.UnusedPathOperation, []},
          {Warning.UnusedRegexOperation, []},
          {Warning.UnusedStringOperation, []},
          {Warning.UnusedTupleOperation, []},
          {Warning.WrongTestFileExtension, []}
        ]
      }
    }
  ]
}
